#include <panel.h>
#include <ncurses.h>
#include <string>
#include <string.h>
#include <vector>
#include <cctype>
#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#define NLINES 30
#define NCOLS 70

void print_in_middle(WINDOW *win, int starty, int startx, int width, char *string, chtype color)
{       int length, x, y;
        float temp;

        if(win == NULL)
                win = stdscr;
        getyx(win, y, x);
        if(startx != 0)
                x = startx;
        if(starty != 0)
                y = starty;
        if(width == 0)
                width = 80;

        length = strlen(string);
        temp = (width - length)/ 2;
        x = startx + (int)temp;
        wattron(win, color);
        mvwprintw(win, y, x, "%s", string);
        wattroff(win, color);
        refresh();
}


void win_show(WINDOW *win, char *label, int label_color)
{       int startx, starty, height, width;

        getbegyx(win, starty, startx);
        getmaxyx(win, height, width);

        box(win, 0, 0);
        mvwaddch(win, 2, 0, ACS_LTEE);
        mvwhline(win, 2, 1, ACS_HLINE, width - 2);
        mvwaddch(win, 2, width - 1, ACS_RTEE);

        print_in_middle(win, 1, 0, width, label, COLOR_PAIR(label_color));
}


void init_wins(WINDOW **wins, int n)
{       int x, y, i;
        char label[80];

        y = 2;
        x = 10;
        for(i = 0; i < n; ++i)
        {       wins[i] = newwin(NLINES, NCOLS, y, x);
                sprintf(label, "Window Number %d", i + 1);
		win_show(wins[i], label, i + 1);
                y += 7;
                x += 7;
        }
/* Show the window with a border and a label */
}

int main()
{       WINDOW *my_wins[2];
        PANEL  *my_panels[2];
        PANEL  *top;
        int ch;

        /* Initialize curses */
        initscr();
        start_color();
        cbreak();
        noecho();
        keypad(stdscr, TRUE);

        /* Initialize all the colors */
	init_pair(1, COLOR_YELLOW, COLOR_BLACK);
        init_pair(2, COLOR_GREEN, COLOR_BLACK);
        init_pair(3, COLOR_BLUE, COLOR_BLACK);
        init_pair(4, COLOR_CYAN, COLOR_BLACK);

        init_wins(my_wins, 2);

        /* Attach a panel to each window */     /* Order is bottom up */
        my_panels[0] = new_panel(my_wins[0]);   /* Push 0, order: stdscr-0 */
        my_panels[1] = new_panel(my_wins[1]);   /* Push 1, order: stdscr-0-1 */

        /* Set up the user pointers to the next panel */
        set_panel_userptr(my_panels[0], my_panels[1]);
        set_panel_userptr(my_panels[1], my_panels[0]);

        /* Update the stacking order. 2nd panel will be on top */
        update_panels();

        /* Show it on the screen */
        attron(COLOR_PAIR(4));
        mvprintw(LINES - 2, 0, "Use tab to browse through the windows (F2 to Exit)");
        attroff(COLOR_PAIR(4));
        doupdate();
	
	char* my_str = new char[256];
	int index = 0;
	while (index < 255) {
		my_str[index] = getch();
		const char* symb = (const char *)(&my_str[index]);
		mvwprintw(my_wins[0], 5, 5 + index, "%s", symb);
		update_panels();
                doupdate();
	
		if (my_str[index] == '\n') {
			break;
		}
		else if (my_str[index] == '\b') {
			my_str[index] = 0;
			--index;
			my_str[index] = 0;
			--index;
		}
		++index;
	}
	my_str[index] = 0;


	refresh();
	std::vector<std::string> data;
	std::string cmd = "";
	bool redir = false;
	int first_redir = 0;
	int last_redir = 0;
        int arg_count = 0;

	for (int i = 0; i != 256; ) {
		std::string word = "";
		while ((isspace(my_str[i])) && (i < 256)) {
			++i;
		}
		while (!(isspace(my_str[i])) && (i < 256)) {	
			word += my_str[i];
			++i;
			
		}
		data.push_back(word);
		++arg_count;
		if (word != "<<" && word != ">>" && word != ">" && word != "<" && !redir) {
			cmd += word;
			cmd += " ";
			first_redir = arg_count;
		}

		else {
			redir = true;
		}

		if (word == "<<" || word == "<" || word == ">>" || word == ">") {
                        last_redir = arg_count;
                }

	}
//	mvwprintw(my_wins[1], 25, 10, "%s", cmd.c_str());

	int x = 5;
	for (size_t i = 0; i != data.size(); ++i) {
		std::string line = data[i];
		mvwprintw(my_wins[1], x, 10, "%s",line.c_str());
		++x;
	}

	pid_t pid;
	bool redirection = false;
	bool is_pipe = false;
	for (size_t i = 0; i != data.size(); ++i) {
		if (i == first_redir)
                        pid = fork();

		if (data[i] == "<") {
			redirection = true;
			if (!pid) {
				int fd = open((data[i + 1]).c_str(), O_RDONLY, 0);
				dup2(fd, 0);
				close(fd);
			}
		}

		else if (data[i] == ">") {
			redirection = true;
			if (!pid) {
				int fd = open((data[i + 1]).c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0666);
				dup2(fd, 1);
				close(fd);
			}
		}

		else if (data[i] == ">>") {
			redirection = true;
			if (!pid) {
				int fd = open((data[i + 1]).c_str(), O_WRONLY | O_CREAT | O_APPEND, 0666);
				dup2(fd, 1);
				close(fd);
			}
		}


		else if (data[i] == "|") {
			is_pipe = true;
			int pfd[2];
			pipe(pfd);
			if (!fork()) {
				dup2(pfd[1], 1);
				close(pfd[0]);
				close(pfd[1]);
				execlp("/bin/sh", "/bin/sh", "-c", data[i - 1].c_str(), NULL);
			}
			close(pfd[1]);
			if (!fork()) {
				dup2(pfd[0], 0);
				close(pfd[0]);
				execlp("/bin/sh", "/bin/sh", "-c", data[i + 1].c_str());
			}
			close(pfd[0]);
			wait(NULL);
			wait(NULL);
               }

		

		//--last_redir
	        if (redirection && i == last_redir && !pid) {
        	        execlp("/bin/sh", "/bin/sh", "-c", cmd.c_str(), NULL);
                	exit(0);
        	}

     	        if (redirection && i == last_redir && pid) {
             		wait(NULL);
		}
		
		if (!redirection && !is_pipe) {         
    		        if (!fork()) {
                                int fd = open("result.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
                                dup2(fd, 1);
                                close(fd);
				execlp("/bin/sh", "/bin/sh", "-c", cmd.c_str(), NULL);
				exit(0);
			}
		}

		
	}


        top = my_panels[1];
        while((ch = getch()) != KEY_F(2))
        {       switch(ch)
                {       case 9:
                                top = (PANEL *)panel_userptr(top);
                                top_panel(top);
                                break;
                }
                update_panels();
                doupdate();
        }
        endwin();
        return 0;
}
